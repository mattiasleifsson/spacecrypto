package spacecrypto.game.util;
import com.badlogic.gdx.Gdx;
public class GameHelper {
    public static float getAttenuation(float attenuationPerSecond) {
        return (float)Math.pow(attenuationPerSecond, Gdx.graphics.getDeltaTime());
    }
}
