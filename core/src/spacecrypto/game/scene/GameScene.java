package spacecrypto.game.scene;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Map;
import spacecrypto.game.coin.Coin;
import com.badlogic.gdx.graphics.Texture;
import java.util.List;
import java.util.ArrayList;
import spacecrypto.game.coin.Coin;
import spacecrypto.game.gameobject.*;
import com.badlogic.gdx.Gdx;
public class GameScene extends Scene {
    private Coin selectedCoin;
    private Map<Coin, Texture> coinTextures;
    private List<ViewableObject> gameObjects;
    private List<ViewableObject> gameObjectsToAdd = new ArrayList<>();
    private Tutorial tutorial;
    private Texture playerTexture = new Texture("resources/Ship.png");
    private Texture shotTexture = new Texture("resources/shot.png");
    private int nbrOfObstaclesDestroyed;
    private BitmapFont font;
    private int padding = 5;
    public GameScene(BitmapFont font, Coin selectedCoin, Map<Coin, Texture> coinTextures) {
        super(font);   
        this.font = font;
        this.selectedCoin = selectedCoin;
        this.coinTextures = coinTextures;
        this.gameObjects = new ArrayList<>();
        this.tutorial = new Tutorial(font, playerTexture, shotTexture);
        this.gameObjects.add(new Player(Gdx.graphics.getWidth() / 2, 100, playerTexture, shotTexture, gameObjectsToAdd));
    }

    public void update() {
        spawnObstacle();

        tutorial.update();
        
        for (ViewableObject v : gameObjects) {
            v.update();
        }

        for (ViewableObject toAdd : gameObjectsToAdd) {
            gameObjects.add(toAdd);
        }
        gameObjectsToAdd.clear();

        for (int i = gameObjects.size() - 1; i > 0; i--) {
            if (gameObjects.get(i).isDead()) {
                removeGameObject(i);
            }
        }
    }

    private void removeGameObject(int index) {
        if (gameObjects.get(index) instanceof Obstacle) {
            nbrOfObstaclesDestroyed++;
        }
        gameObjects.remove(index);
    }

    private void spawnObstacle() {
        if (Math.random() < Gdx.graphics.getDeltaTime() / 2f) {
            gameObjects.add(new Obstacle(
                (float)Math.random() * Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight() + 50,
                coinTextures.get(selectedCoin),
                gameObjects,
                gameObjectsToAdd));
        }
    }

    public void draw(SpriteBatch batch) {
        tutorial.draw(batch);
        for (ViewableObject g : gameObjects) {
            g.draw(batch);
        }
        batch.draw(coinTextures.get(selectedCoin),
                   padding,
                   Gdx.graphics.getHeight() - coinTextures.get(selectedCoin).getHeight() - padding);
        font.setColor(1, 1, 1, 1);
        font.draw(batch,
                  "x" + nbrOfObstaclesDestroyed,
                  coinTextures.get(selectedCoin).getWidth() + padding,
                  Gdx.graphics.getHeight() - padding - 5); 
    }
}
