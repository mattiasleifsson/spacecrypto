package spacecrypto.game.scene;

import java.util.Set;
import java.util.HashSet;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.graphics.Color;
import spacecrypto.game.gameobject.TextObject;
public class SceneHandler {
    private Set<Scene> scenes = new HashSet<>();
    private Scene current;
    private boolean isRunning;
    private Texture transition;
    private float transitionDuration = 2;
    private float transitionTimer;
    private boolean isFadingOut;
    private Scene nextScene;
    private TextObject transitionText;
    private BitmapFont font;

    public SceneHandler(BitmapFont font) {
        this.font = font;
        transition = new Texture("resources/transition.png");

        transitionText = new TextObject(
            Gdx.graphics.getWidth() / 2,
            (int)(Gdx.graphics.getHeight() * 0.65f),
            "SpaceCrypto",
            font);
        transitionText.setColor(new Color(1, 1, 1, MathUtils.clamp(transitionTimer / transitionDuration, 0, 1)));
    }

    public void start(Scene scene) {
        current = scene;
        isRunning = true;
    }

    public void changeScene(Scene scene) {
        scenes.add(scene);
        nextScene = scene;
        isFadingOut = true;
        transitionTimer = 0.1f;
    }

    public void update() {
        if (isRunning && transitionTimer <= 0) {
            current.update();
        } else {
            transition();
        }
    }

    private void transition() {
        if (isFadingOut) {
            transitionTimer += Gdx.graphics.getDeltaTime();
            if (transitionTimer > transitionDuration) {
                isFadingOut = false;
                current = nextScene;
                nextScene = null;
            }
        } else {
            transitionTimer -= Gdx.graphics.getDeltaTime();
        }
    }

    public void draw(SpriteBatch batch) {
        if (isRunning) {
            current.draw(batch);
        }
        float alpha = MathUtils.clamp(transitionTimer / transitionDuration, 0, 1);
        batch.setColor(1, 1, 1, alpha);
        batch.draw(transition, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        transitionText.setColor(new Color(218f/255, 165f/255, 32f/255, MathUtils.clamp(transitionTimer / transitionDuration - 0.4f, 0, 1)));
        transitionText.setSize((float)Math.pow(alpha, 3) + 2);
        transitionText.draw(batch);
    }

}
