package spacecrypto.game.scene;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.List;
import java.util.ArrayList;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import spacecrypto.game.gameobject.*;
import spacecrypto.game.coin.*;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;
import java.util.Map;
public class SelectCoinScene extends Scene {
    private List<GameObject> gameObjects;
    private Map<Coin, Texture> coinTextures;
    private int coinsPerRow = 5;
    private SceneHandler handler;
    public SelectCoinScene(Map<Coin, Texture> coinTextures, BitmapFont font, SceneHandler handler) {
        super(font);
        gameObjects = new ArrayList<>();
        this.coinTextures = coinTextures;
        this.handler = handler;
        createCoinButtons();
    }

    private void createCoinButtons() {
        int i = 0;
        for (Coin c : coinTextures.keySet()) {
            Texture coinTexture = coinTextures.get(c);
            float x = getCoinButtonXPosition(i);
            float y = getCoinButtonYPosition(i, coinTexture.getHeight());
            gameObjects.add(new Button(x, y, coinTexture, createCommand(c)));
            i++;
        }
    }

    private float getCoinButtonXPosition(int coinNumber) {
        float offset = 0.5f;
        int column = coinNumber % coinsPerRow;
        return ((float)(offset + column) / coinsPerRow) * Gdx.graphics.getWidth();
    }

    private float getCoinButtonYPosition(int coinNumber, int coinTextureHeight) {
        float offset = coinTextureHeight * 0.5f;
        float startOffset = coinTextureHeight * 1;
        int row = coinNumber / coinsPerRow;
        return Gdx.graphics.getHeight() - (row * coinTextureHeight + offset * row + startOffset);
    }

    private Command createCommand(final Coin c) {
        return new Command() {
            public void execute() {
                selectCoin(c);
            }
        };
    }

    private void selectCoin(Coin c) {
        handler.changeScene(new GameScene(font, c, coinTextures));
    }

    public void update() {
        for (GameObject gameObject : gameObjects) {
            gameObject.update();
        }
    }

    public void draw(SpriteBatch batch) {
        for (GameObject gameObject : gameObjects) {
            gameObject.draw(batch);
        }
    }
}
