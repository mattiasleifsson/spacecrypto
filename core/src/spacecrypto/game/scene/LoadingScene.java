package spacecrypto.game.scene;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import spacecrypto.game.coin.*;
import com.badlogic.gdx.graphics.Texture;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import spacecrypto.game.gameobject.TextObject;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
public class LoadingScene extends Scene {
    private Map<Coin, Texture> textures;
    private Map<Coin, String> paths;
    private int progress;
    private TextObject instructionText;
    private boolean doneLoading = false;
    private SceneHandler handler;
    public LoadingScene(SceneHandler handler, BitmapFont font) {
        super(font);
        textures = new TreeMap<>();
        paths = new HashMap<>();
        startLoadingThread();
        instructionText = new TextObject(Gdx.graphics.getWidth() / 2, 100, "Press enter to continue", font, 0.5f);
        instructionText.setColor(new Color(218f/255, 165f/255, 32f/255, 1));
        this.handler = handler;
    }

    private void startLoadingThread() {
        Thread loadingThread = new Thread() {
            public void run() {
                loadCoinPaths();
            }
        };
        loadingThread.start();
    }

    private void loadCoinPaths() {
        List<Coin> top50Coins = new TopCoinsHandler().getTopCoins(50);
        CoinImageHandler coinImageHandler = new CoinImageHandler();
        for (Coin c : top50Coins) {
            paths.put(c, coinImageHandler.getCoinImagePath(c));
            progress += 1;
        }
    }

    public void update() { 
        if (progress == 50) {
            loadCoinTextures();
            progress++;
            doneLoading = true;
        }

        instructionText.update();

        if (doneLoading && Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
            handler.changeScene(new SelectCoinScene(textures, font, handler));
        }
    }

    private void loadCoinTextures() {
        for (Coin c : paths.keySet()) {
            textures.put(c, new Texture(paths.get(c)));
        }
    }

    public void draw(SpriteBatch batch) {
        font.setColor(218f/255, 165f/255, 32f/255, 1);
        GlyphLayout layout = new GlyphLayout(font, (int)((progress / 51f) * 100) + "%");
        font.draw(batch, layout, Gdx.graphics.getWidth()  / 2 - layout.width / 2, Gdx.graphics.getHeight() - 200);
        layout = new GlyphLayout(font, "Loading");
        font.draw(batch, layout, Gdx.graphics.getWidth()  / 2 - layout.width / 2, Gdx.graphics.getHeight() - 150);
        if (doneLoading) {
            instructionText.draw(batch);
        }
    }
}
