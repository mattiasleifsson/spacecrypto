package spacecrypto.game.scene;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
public abstract class Scene {
    protected BitmapFont font;
    public Scene(BitmapFont font) {
        this.font = font;
    }
    abstract void update();
    abstract void draw(SpriteBatch batch);
}                
