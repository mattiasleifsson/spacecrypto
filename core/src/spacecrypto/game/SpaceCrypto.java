package spacecrypto.game;

import spacecrypto.game.gameobject.*;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import spacecrypto.game.scene.*;
import com.badlogic.gdx.files.FileHandle;

public class SpaceCrypto extends ApplicationAdapter {
	SpriteBatch batch;
    BitmapFont font;
    SceneHandler sceneHandler;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
        font = new BitmapFont(new FileHandle("resources/font32.fnt"));
        font.setColor(218f/255, 165f/255, 32f/255, 1);
        sceneHandler = new SceneHandler(font);
        LoadingScene loadingScene = new LoadingScene(sceneHandler, font);
        sceneHandler.start(loadingScene);
	}

	@Override
	public void render() {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sceneHandler.update();

		batch.begin();
        sceneHandler.draw(batch);
		batch.end();
	}
	
	@Override
	public void dispose() {
		batch.dispose();
        font.dispose();
	}
}
