package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;
public class GameObject {
    protected float x;
    protected float y;
    private boolean isDead = false;

    public GameObject(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void update() {
        
    }

    public void die() {
        isDead = true;
    }

    public boolean isDead() {
        return isDead;
    }

    public void draw(SpriteBatch batch) {
        batch.setColor(1, 1, 1, 1);
    }
}
