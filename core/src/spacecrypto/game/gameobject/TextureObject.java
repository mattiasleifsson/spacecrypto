package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;
public class TextureObject extends ViewableObject {
    protected Texture texture;
    protected float rotation;
    private int textureX, textureY, textureWidth, textureHeight;

    public TextureObject(float x, float y, Texture texture,
                        int textureX, int textureY, int textureWidth, int textureHeight) {
        super(x, y);
        this.texture = texture;
        this.textureX = textureX;
        this.textureY = textureY;
        this.textureWidth = textureWidth;
        this.textureHeight = textureHeight;
    }

    public TextureObject(float x, float y, Texture texture) {
        this(x, y, texture, 0, 0, texture.getWidth(), texture.getHeight());
    }

    public void update() {

    }

    private float getOriginX() {
        return textureX + textureWidth / 2;
    }

    private float getOriginY() {
        return textureY + textureHeight / 2;
    }

    public float getWidth() {
        return textureWidth * size;
    }

    public float getHeight() {
        return textureHeight * size;
    }

    public boolean contains(float x, float y) {
        return x > this.x - (getWidth() / 2) &&
               x < this.x + (getWidth() / 2) &&
               y > this.y - (getWidth() / 2) &&
               y < this.y + (getWidth() / 2);
    }
    
    public void draw(SpriteBatch batch) {
        super.draw(batch);
        batch.setColor(color);
        batch.draw(texture, x - getOriginX(), y - getOriginY(),
                   textureWidth / 2, textureHeight / 2,
                   textureWidth, textureHeight,
                   size, size,
                   rotation,
                   textureX, textureY, textureWidth, textureHeight,
                   false, false);
        batch.setColor(1, 1, 1, 1);
    }
}
