package spacecrypto.game.gameobject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import java.util.List;
import com.badlogic.gdx.math.MathUtils;
public class Obstacle extends TextureObject {
    private List<ViewableObject> gameObjects;
    private List<ViewableObject> toAdd;
    public Obstacle(float x, float y, Texture texture, List<ViewableObject> gameObjects, List<ViewableObject> toAdd) {
        super(x, y, texture);
        this.gameObjects = gameObjects;
        this.toAdd = toAdd;
    }

    public void update() {
        y -= Gdx.graphics.getDeltaTime() * 30;

        for (ViewableObject v : gameObjects) {
            if (!equals(v) && v instanceof Shot && collision(v)) {
                explode();
                die();
                v.die();
            }
        }
    }

    private void explode() {
        for (int i = 0; i < 10; i++) {
            float pieceX = x + MathUtils.lerp(-10, 10, (float)Math.random());
            float pieceY = y + MathUtils.lerp(-10, 10, (float)Math.random());
            toAdd.add(new Piece(pieceX, pieceY, texture));
        }
    }
}
