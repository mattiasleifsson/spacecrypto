package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.Gdx;
import spacecrypto.game.util.GameHelper;
public class Piece extends TextureObject {
    private float speed = 100;
    private float speedDirection;
    private float rotationSpeed;
    public Piece(float x, float y, Texture texture) {
        super(x, y, texture,
             (int)MathUtils.lerp(0, texture.getWidth() / 2, (float)Math.random()),
             (int)MathUtils.lerp(0, texture.getHeight() / 2, (float)Math.random()),
             (int)MathUtils.lerp(texture.getWidth() / 5, texture.getWidth() / 2, (float)Math.random()),
             (int)MathUtils.lerp(texture.getHeight() / 5, texture.getHeight() / 2, (float)Math.random()));
             this.speedDirection = (float)Math.random() * 360;
             this.rotationSpeed = MathUtils.lerp(-800, 800, (float)Math.random());
    }

    public void update() {
        x += speed * Gdx.graphics.getDeltaTime() * MathUtils.cosDeg(speedDirection);
        y += speed * Gdx.graphics.getDeltaTime() * MathUtils.sinDeg(speedDirection);
        speed *= GameHelper.getAttenuation(0.5f);
        rotation += rotationSpeed * Gdx.graphics.getDeltaTime();
        rotationSpeed *= GameHelper.getAttenuation(0.5f);
        setColor(color.r, color.g, color.b, color.a * GameHelper.getAttenuation(0.5f));
    }
}
