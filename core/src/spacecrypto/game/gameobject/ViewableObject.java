package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.Color;
public abstract class ViewableObject extends GameObject {
    protected float size;
    protected Color color;
    protected float rotation;
    public ViewableObject(float x, float y) {
        super(x, y);
        this.size = 1;
        this.color = new Color(1, 1, 1, 1);
    }

    public void setSize(float size) {
        this.size = size;
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = new Color(r, g, b, a);
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    abstract float getWidth();
    abstract float getHeight();

    public boolean collision(ViewableObject other) {
        return !(x - getWidth() / 2 > other.x + other.getWidth() / 2 ||
               x + getWidth() / 2 < other.x ||
               y  - getHeight() / 2 > other.y + other.getHeight() / 2||
               y + getHeight() / 2 < other.y);
    }
}
