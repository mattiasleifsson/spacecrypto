package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import java.util.List;
import java.util.ArrayList;

public class Tutorial extends GameObject {
    
    private MovementTutorial movementTutorial;
    private ShootTutorial shootTutorial;
    private ObjectiveTutorial objectiveTutorial;
    public Tutorial(BitmapFont font, Texture playerTexture, Texture playerShot) {
        super(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        movementTutorial = new MovementTutorial(x - Gdx.graphics.getWidth() / 4, y + Gdx.graphics.getHeight() / 7, font, playerTexture);
        shootTutorial = new ShootTutorial(x + Gdx.graphics.getWidth() / 4, y + Gdx.graphics.getHeight() / 7, font, playerTexture, playerShot);
        objectiveTutorial = new ObjectiveTutorial(x, y - 50, font);
    }

    public void update() {}

    public void draw(SpriteBatch batch) {
        movementTutorial.draw(batch);
        shootTutorial.draw(batch);
        objectiveTutorial.draw(batch);
    }

    private abstract class SubTutorial extends GameObject {
        protected List<ViewableObject> tutorialObjects;
        public SubTutorial(float x, float y) {
            super(x, y);
            this.tutorialObjects = new ArrayList<>();
        }

        public void update() {
            for (GameObject g : tutorialObjects) {
                g.update();
            }
        }

        public void draw(SpriteBatch batch) {
            for (GameObject g : tutorialObjects) {
                g.draw(batch);
            }
        }

        abstract protected void addTutorialObjects();

        protected void addTutorialObject(ViewableObject v) {
            v.setColor(1, 1, 1, 0.3f);
            v.setSize(0.8f);
            tutorialObjects.add(v);
        }
    }

    private class MovementTutorial extends SubTutorial {
        private Texture playerTexture;
        private BitmapFont font;
        public MovementTutorial(float x, float y, BitmapFont font, Texture playerTexture) {
            super(x, y);
            this.playerTexture = playerTexture;
            this.font = font;
            addTutorialObjects();
        }

        protected void addTutorialObjects() {
            addTutorialObject(new TextObject(x, y + playerTexture.getHeight() + 50, "Movement:", font));
            addTutorialObject(new TextureObject(x, y, playerTexture));
            addTutorialObject(new TextObject(x + playerTexture.getWidth() / 2 + 50, y, "D", font));
            addTutorialObject(new TextObject(x, y - playerTexture.getHeight() / 2 - 20, "S", font));
            addTutorialObject(new TextObject(x - playerTexture.getWidth() / 2 - 50, y, "A", font));
            addTutorialObject(new TextObject(x, y + playerTexture.getHeight() / 2 + 70, "W", font));
        }
    }

    private class ShootTutorial extends SubTutorial {
        private Texture playerTexture;
        private Texture playerShot;
        private BitmapFont font;
        public ShootTutorial(float x, float y, BitmapFont font, Texture playerTexture, Texture playerShot) {
            super(x, y);
            this.font = font;
            this.playerTexture = playerTexture;
            this.playerShot = playerShot;
            addTutorialObjects();
        }

        protected void addTutorialObjects() {
            addTutorialObject(new TextObject(x, y + playerTexture.getHeight() + 50, "Shoot:", font));
            addTutorialObject(new TextureObject(x, y, playerTexture));
            addTutorialObject(new TextureObject(x, y + playerTexture.getHeight() / 2 + 20, playerShot));
            addTutorialObject(new TextObject(x, y + playerTexture.getHeight() / 2 + 70, "Space", font));
        }
    }

    private class ObjectiveTutorial extends SubTutorial {
        private BitmapFont font;
        public ObjectiveTutorial(float x, float y, BitmapFont font) {
            super(x, y);
            this.font = font;
            addTutorialObjects();
        }

        protected void addTutorialObjects() {
            addTutorialObject(new TextObject(x, y + 30, "Objective:", font));
            addTutorialObject(new TextObject(x, y - 10, "Do not let the coins reach the \nend of the screen", font));
        }
    }
}

