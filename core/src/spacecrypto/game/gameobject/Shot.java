package spacecrypto.game.gameobject;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.Gdx;
public class Shot extends TextureObject {
    private float speedX;
    private float speedY;
    public Shot(float x, float y, Texture texture, float rotation, float speedX, float speedY) {
        super(x, y, texture);
        this.speedX = speedX;
        this.speedY = speedY;
        this.rotation = rotation;
    }

    public void update() {
        move();
    }

    private void move() {
        x += speedX * Gdx.graphics.getDeltaTime();
        y += speedY * Gdx.graphics.getDeltaTime();
    }
}
