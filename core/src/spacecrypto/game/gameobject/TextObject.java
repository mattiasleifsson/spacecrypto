package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
public class TextObject extends ViewableObject {
    private String text;
    private GlyphLayout layout;
    private float blinkInterval;
    private float blinkTimer;
    private boolean isHidden;
    private BitmapFont font;
    public TextObject(float x, float y, String text, BitmapFont font) {
        super(x, y);
        this.text = text;
        this.layout = new GlyphLayout(font, text);
        this.blinkInterval = -1;
        this.font = font;
    }

    public TextObject(float x, float y, String text, BitmapFont font, float blinkInterval) {
        this(x, y, text, font);
        this.blinkInterval = blinkInterval;
    }

    public void update() {
        if (blinkInterval > 0) {
            blinkTimer += Gdx.graphics.getDeltaTime();
            if (blinkTimer > blinkInterval) {
                blinkTimer = 0;
                isHidden = !isHidden;
            }
        }
    }

    private float getOriginX() {
        return layout.width / 2;
    }

    private float getOriginY() {
        return layout.height / 2;
    }

    public float getWidth() {
        return layout.width;
    }

    public float getHeight() {
        return layout.height;
    }

    public void draw(SpriteBatch batch) {
        if (!isHidden) {
            super.draw(batch);
            font.setColor(color);
            font.getData().setScale(size);
            layout.setText(font, text);
            font.draw(batch, layout, x - getOriginX(), y - getOriginY());
            font.getData().setScale(1);
        }
    }
}
