package spacecrypto.game.gameobject;
public interface Command {
    void execute();
}
