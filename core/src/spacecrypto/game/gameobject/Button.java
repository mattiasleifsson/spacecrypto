package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;
import static com.badlogic.gdx.math.MathUtils.lerp;
import com.badlogic.gdx.Input;
public class Button extends TextureObject {

    private Command pressed;
    
    public Button(float x, float y, Texture texture, Command pressed) {
        super(x, y, texture);
        this.pressed = pressed;
    }

    @Override
    public void update() {
        if (mouseHoverOver()) {
            size = lerp(size, 0.8f, 0.1f);
        } else {
            size = lerp(size, 1, 0.1f);
        }

        if (mouseHoverOver() && mousePressed()) {
            size = 1f;
            pressed.execute();
        }
    }

    private boolean mousePressed() {
        return Gdx.input.isButtonPressed(Input.Buttons.LEFT);
    }

    private boolean mouseHoverOver() {
        return contains(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY());
    }
}
