package spacecrypto.game.gameobject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import java.util.List;
import com.badlogic.gdx.math.MathUtils;
import spacecrypto.game.util.GameHelper;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
public class Player extends TextureObject {
    private float speedX;
    private float speedY;
    private float acceleration = 1000;
    private float attenuationPerSecond = 0.0461f;
    private float rotationMultiplier = 0.05f;
    private float shootCooldown = 1;
    private float shootTimer = 0;
    private Texture shotTexture;
    private List<ViewableObject> gameObjectsToAdd;
    public Player(float x, float y, Texture texture, Texture shotTexture, List<ViewableObject> gameObjectsToAdd) {
        super(x, y, texture);
        this.shotTexture = shotTexture;
        this.gameObjectsToAdd = gameObjectsToAdd;
    }

    public void update() {
        move();
        shoot(gameObjectsToAdd);
    }

    private void move() {
        if (Gdx.input.isKeyPressed(Keys.W)) {
            speedY += acceleration * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyPressed(Keys.D)) {
            speedX += acceleration * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyPressed(Keys.S)){
            speedY -= acceleration * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyPressed(Keys.A)) {
            speedX -= acceleration * Gdx.graphics.getDeltaTime();
        }

        speedX *= GameHelper.getAttenuation(attenuationPerSecond);
        speedY *= GameHelper.getAttenuation(attenuationPerSecond);

        x += speedX * Gdx.graphics.getDeltaTime();
        y += speedY * Gdx.graphics.getDeltaTime();
        rotation = -speedX * rotationMultiplier;
    }

    private void shoot(List<ViewableObject> gameObjectsToAdd) {
        shootTimer += Gdx.graphics.getDeltaTime();
        if (shootTimer > shootCooldown && Gdx.input.isKeyPressed(Keys.SPACE)) {
            shootTimer = 0;
            float shotX = x + (getWidth() / 2) * MathUtils.cosDeg(rotation + 90);
            float shotY = y + (getHeight() / 2) * MathUtils.sinDeg(rotation + 90);
            float shotRotation = rotation;
            float baseShotSpeed = 300;
            float shotSpeedX = speedX +
                               baseShotSpeed * MathUtils.cosDeg(rotation + 90);
            float shotSpeedY = MathUtils.clamp(speedY, 0, speedY) +
                               baseShotSpeed * MathUtils.sinDeg(rotation + 90);
            gameObjectsToAdd.add(new Shot(shotX, shotY, shotTexture, shotRotation, shotSpeedX, shotSpeedY));
        }
    }
}
