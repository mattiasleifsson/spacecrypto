package spacecrypto.game.coin;

import java.net.URL;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.util.Arrays;
public class CoinImageHandler {
    public CoinImageHandler() {

    }

    public String getCoinImagePath(Coin coin) {
        String path = "resources/coinimages/" + coin.getSymbol().toLowerCase() + ".png";
        if (imageAlreadyDownloaded(path)) {
            return path;
        }

        try {
            URL url = new URL("https://github.com/cjdowner/cryptocurrency-icons/raw/master/32/icon/" + coin.getSymbol().toLowerCase() + ".png");
            InputStream inputStream = new Webpage(url).get();
            if (inputStream == null) {
                return "Image path can not be retrieved";
            }
            byte[] image = getImageBytesFromInputStream(inputStream);
            saveImage(image, path);
        } catch (IOException e) {
            System.out.println(e);
        }
        return path;
    }

    private boolean imageAlreadyDownloaded(String path) {
        return Files.exists(Paths.get(path));
    }

    private byte[] getImageBytesFromInputStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int n = 0;
        while ((n = inputStream.read(buffer)) != -1) {
            out.write(buffer, 0, n);
        }
        inputStream.close();
        out.close();
        return out.toByteArray();
    }

    private void saveImage(byte[] image, String path) throws IOException, FileNotFoundException {
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        fileOutputStream.write(image);
        fileOutputStream.close();
    }
}
