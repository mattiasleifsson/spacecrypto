package spacecrypto.game.coin;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.InputStream;
import java.io.IOException;
import java.io.File;
public class TopCoinsHandler {
    private static final String WEBPAGE = "https://api.coinmarketcap.com/v1/ticker/?limit=";
    private InputStream inputStream;

    public List<Coin> getTopCoins(int lowerBound) {
        return getTopCoins(lowerBound, true);
    }

    public List<Coin> getTopCoins(int lowerBound, boolean offline) {
        if (lowerBound < 1 || lowerBound > 50) {
            throw new IllegalArgumentException("lowerbound has to be between 1 and 50");
        }

        List<Coin> topCoins = new ArrayList<>();

        try {
            Scanner scanner = getOfflineScanner();
            if (!offline) {
                scanner = getWebpage(WEBPAGE + lowerBound);
            }
            topCoins = readCoins(scanner, lowerBound);
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return topCoins;
    }

    private Scanner getWebpage(String webpage) {
        Scanner scanner = getOfflineScanner();
        try {
            scanner = new Scanner(new Webpage(new URL(webpage)).get());
        } catch (Exception e) {
            System.out.println(e);
        }
        return scanner;
    }

    private Scanner getOfflineScanner() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("resources/offlinecoins.txt"));
        } catch (IOException e) {
            System.out.println(e);
        }
        return scanner;
    }

    private List<Coin> readCoins(Scanner scanner, int nbrOfCoinsToRead) {
        List<Coin> coins = new ArrayList<>();
        while (scanner.hasNextLine() && coins.size() < nbrOfCoinsToRead) {
            coins.add(readCoin(scanner));
        }
        return coins;
    }

    private Coin readCoin(Scanner scanner) {
        Map<String, String> coinProperties = new HashMap<>();
        String line = "";
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (line.contains("}")) {
                break;
            } else if (line.contains("\"")) {
                coinProperties.put(getKey(line), getValue(line));
            }
        }
        return getCoinFromMap(coinProperties);
    }

    private Coin getCoinFromMap(Map<String, String> coinProperties) {
        return new Coin(
            coinProperties.get("name"),
            coinProperties.get("symbol"),
            Double.parseDouble(coinProperties.get("price_usd")),
            Integer.parseInt(coinProperties.get("rank")));
    }

    private String getKey(String line) {
        return getPropety(line, 0);
    }

    private String getValue(String line) {
        return getPropety(line, 1);
    }

    private String getPropety(String line, int i) {
        return line
            .split(":")[i]
            .replace("\"", "")
            .replace(",", "")
            .trim();
    }
    
}
