package spacecrypto.game.coin;

public class Coin implements Comparable<Coin> {
    private String name, symbol;
    private double value;
    private int rank;

    public Coin(String name, String symbol, double value, int rank) {
        this.name = name;
        this.symbol = symbol;
        this.value = value;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getValue() {
        return value;
    }

    public int getRank() {
        return rank;
    }

    public int compareTo(Coin other) {
        return Integer.compare(getRank(), other.getRank());
    }
}
