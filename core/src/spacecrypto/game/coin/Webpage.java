package spacecrypto.game.coin;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;

public class Webpage {
    private URL url;
    public Webpage(URL url) {
        this.url = url;
    }

    public InputStream get() {
        BufferedInputStream inputStream = null;
        try {
            inputStream = new BufferedInputStream(url.openStream());
        } catch (IOException e) {
            System.out.println(e);
        }
        return inputStream;
    }
}
