import spacecrypto.game.coin.*;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import static org.junit.Assert.*;
import org.junit.*;
public class TestTopCoinsHandler {
    private TopCoinsHandler handler = new TopCoinsHandler();
    private static final String VALID_CHARACTERS = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @Test
    public void topCoinsHandlerShouldHaveGetTopCoinsMethod() {
        handler.getTopCoins(50);
    }

    @Test
    public void getTopCoinsShouldReturnCorrectNbrOfCoins() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        assertEquals(10, top10Coins.size());
    }

    @Test
    public void getTopCoinsShouldContainBitcoin() {
        List<Coin> top30Coins = handler.getTopCoins(30);
        boolean top30CoinsContainsBitcoin = false;
        for (Coin c : top30Coins) {
            if (c.getName().equals("Bitcoin")) {
                top30CoinsContainsBitcoin = true;
            }
        }
        assertTrue("Failure - Bitcoin was not in top 30 coins", top30CoinsContainsBitcoin);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getTopCoinsLowerBoundShouldBeBetween1And50() {
        handler.getTopCoins(0);
        handler.getTopCoins(51);
    }

    @Test
    public void getTopCoinsShouldContainToDuplicateCoinNames() {
        List<Coin> top30Coins = handler.getTopCoins(30);
        Set<String> coinNamesAlreadyAppeared = new HashSet<>();
        for (Coin c : top30Coins) {
            assertFalse("Failure - getTopCoins returned duplicate coin", coinNamesAlreadyAppeared.contains(c.getName()));
            coinNamesAlreadyAppeared.add(c.getName());
        }
    }

    @Test
    public void getTopCoinsShouldReturnCoinsWithValidCharactersInName() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - Coin name contains invalid character, name: " + c.getName(), onlyContainsValidCharacters(c.getName(), VALID_CHARACTERS));
        }
    }

    @Test
    public void getTopCoinsShouldReturnCoinsWithValidCharactersInSymbol() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - Coin symbol contains invalid character, name: " + c.getSymbol(), onlyContainsValidCharacters(c.getSymbol(), VALID_CHARACTERS));
        }
    }

    @Test
    public void getTopCoinsShouldNotReturnCoinsWithNegativeValue() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - Coin value was invalid, value: " + c.getValue(), c.getValue() >= 0);
        }
    }

    @Test
    public void getTopCoinsShouldNotReturnCoinsWithEmptyNames() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - Coin name was empty", c.getName().length() > 0);
        }
    }

    @Test
    public void getTopCoinsShouldNotReturnCoinsWithEmptySymbols() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - Coin symbol was empty", c.getSymbol().length() > 0);
        }
    }

    @Test
    public void getTopCoinsShouldWorkOnline() {
        List<Coin> top10CoinsOnline = handler.getTopCoins(10, false);
        List<Coin> top10CoinsOffline = handler.getTopCoins(10, true);
        boolean samePrice = true;
        for (int i = 0; i < 10; i++) {
            if (Math.abs(top10CoinsOnline.get(i).getValue() - top10CoinsOffline.get(i).getValue()) > 0.1d) {
                samePrice = false;
            }
        }
        assertFalse("Offline and online coins are the same", samePrice);
    }

    @Test
    public void getTopCoinsShouldReturnCoinsWithValidRanks() {
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertTrue("Failure - coin rank was not between 1 and 10", c.getRank() > 0 && c.getRank() < 11);
        }
    }

    @Test
    public void getTopCoinsShouldReturnCoinsWithDifferentRanks() {
        Set<Integer> ranks = new HashSet<>();
        List<Coin> top10Coins = handler.getTopCoins(10);
        for (Coin c : top10Coins) {
            assertFalse("Failure - duplicate rank", ranks.contains(c.getRank()));
            ranks.add(c.getRank());
        }
    }

    @Test
    public void testTestOnlyContainsValidCharacters() {
        assertTrue("Failure - Method should return true", onlyContainsValidCharacters("abertgjIOHJEN", VALID_CHARACTERS));
        assertFalse("Failure - Method should return true", onlyContainsValidCharacters("eujohEV9FEfeeqf", VALID_CHARACTERS));
    }

    private boolean onlyContainsValidCharacters(String s, String validCharacters) {
        for (char validChar : validCharacters.toCharArray()) {
            s = s.replace(String.valueOf(validChar), "");
        }
        System.out.println(s);
        return s.length() == 0;
    }
}
