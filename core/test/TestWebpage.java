import spacecrypto.game.coin.Webpage;
import static org.junit.Assert.*;
import org.junit.*;
import java.util.Scanner;
import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.nio.file.Paths;
public class TestWebpage {
    private Webpage webpage;

    @Test
    public void webpageShouldHaveGetMethod()  throws MalformedURLException {
        webpage = new Webpage(new URL("https://github.com/cjdowner/cryptocurrency-icons/blob/master/32/icon/btc.png"));
        webpage.get();
    }

    @Test
    public void getMethodShouldReturnCorrectValue() throws MalformedURLException {
        webpage = new Webpage(Paths.get("testresources/webpage").toUri().toURL());
        InputStream is = webpage.get();
        Scanner scanner = new Scanner(webpage.get());
        String line = scanner.nextLine();
        try {
            is.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        assertEquals("This is a text.", line);
    }
}
