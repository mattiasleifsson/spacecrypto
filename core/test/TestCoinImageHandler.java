import spacecrypto.game.coin.*;
import static org.junit.Assert.*;
import org.junit.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
public class TestCoinImageHandler {
    private CoinImageHandler handler = new CoinImageHandler();
    private Coin bitcoin = new Coin("Bitcoin", "BTC", 10000, 1);

    @Test
    public void coinImageHandlerShouldHaveGetCoinImagePathMethod() {
        handler.getCoinImagePath(bitcoin);
    }
    
    @Test
    public void getCoinImagePathShouldDownloadImageIfNotInImageFolder() {
        deleteAllCoinImages();
        String path = handler.getCoinImagePath(bitcoin);
        assertTrue("Failure - image does not exist at path", Files.exists(Paths.get(path)));
    }

    @Test
    public void getCoinImagePathShouldNotDownloadImageIfItAlreadyExists() throws IOException {
        String path = handler.getCoinImagePath(bitcoin);
        FileTime lastModifiedTime1 = Files.getLastModifiedTime(Paths.get(path));
        path = handler.getCoinImagePath(bitcoin);
        FileTime lastModifiedTime2 = Files.getLastModifiedTime(Paths.get(path));
        assertTrue("Failure - a new image was downloaded", lastModifiedTime1.equals(lastModifiedTime2));
    }

    @Test
    public void getCoinImagePathShouldReturnMessageIfItCanNotGetPath() {
        Coin dummyCoin = new Coin("Non-existing", "Non-existing", -1, 0);
        assertEquals("Image path can not be retrieved", handler.getCoinImagePath(dummyCoin));
    }

    private void deleteAllCoinImages() {
        File directory = new File("resources/coinimages");
        for (String fileName : directory.list()) {
            new File("resources/coinimages/" + fileName).delete();
        }
    }
}
