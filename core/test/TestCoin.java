import spacecrypto.game.coin.Coin;
import static org.junit.Assert.*;
import org.junit.*;
public class TestCoin {
    private Coin coin;

    @Test
    public void coinShouldHaveName() {
        coin = new Coin("Bitcoin", "BTC", 10000, 1);
        assertEquals("Bitcoin", coin.getName());
    }

    @Test
    public void coinShouldHaveSymbol() {
        coin = new Coin("Bitcoin", "BTC", 10000, 1);
        assertEquals("BTC", coin.getSymbol());
    }

    @Test
    public void coinShouldHaveValue() {
        coin = new Coin("Bitcoin", "BTC", 10000, 1);
        assertEquals(10000, coin.getValue(), 0.1);
    }

    @Test
    public void coinShouldHaveRank() {
        coin = new Coin("Bitcoin", "BTC", 10000, 1);
        assertEquals(1, coin.getRank());
    }

    @Test
    public void compareToShouldCompareRank() {
        Coin coin1 = new Coin("Bitcoin", "BTC", 10000, 1);
        Coin coin2= new Coin("OtherCoin", "ABC", 21321321, 5);
        Coin coin3= new Coin("SameCoin", "SAME", 546, 1);
        assertTrue("Failure - compareTo should be less that zero", coin1.compareTo(coin2) < 0);
        assertTrue("Failure - compareTo should be zero", coin1.compareTo(coin3) == 0);
    }
}
